'use strict';

var co = require('co');
var request = require('request');
var cheerio = require('cheerio');
var format = require('util').format;
var flatten = require('underscore').flatten;
var template = require('underscore').template;
var path = require('path');
var fs = require('fs');

const DOMAIN = 'http://sprosi.d3.ru';
const START_URL = 'new/pages';
const COMMENT_URL = 'comments/%s';
const NUM_PAGES = 10;

var extractLinks = function (url) {
    return function (cb) {
        var result = [];
        request(url, function (err, response, body) {
            if (err || response.statusCode != 200)
                cb(new Error(format('%s not found', url)));

            let $ = cheerio.load(body);
            $('.post').each(function (i, el) {
                let id = $(el).attr('id').slice(1);
                let title = $(el).find('h3').text().replace('[', '').replace(']', '');

                if (['(', '{'].indexOf(title[0]) == -1)
                    result.push({
                        id: id,
                        title: title,
                        href: format(COMMENT_URL, id),
                        checked: /автор прошел проверку на подлинность/i.test($(el).find('.moderator').text()),
                        golden: $(el).find('.stars').length > 0,
                        rating: $(el).find('.vote_result').text().trim(),
                        comments: $(el).find('.b-post_comments_links').text().split(' ')[0],
                        user: $(el).find('.c_user').text(),
                        user_href: $(el).find('.c_user').attr('href'),
                        last_updated: $(el).find('.js-date').data('epoch_date')
                    })
            });

            cb(null, result)
        }.bind(this))

    }
};

co(function*() {
    var links = [];
    for (var i = 1; i <= NUM_PAGES; i++)
        links.push(extractLinks(format('%s/%s/%s', DOMAIN, START_URL, i)));

    fs.writeFile(path.join(__dirname, '..', 'result.html'), '');
    flatten(yield links)
        .sort(function (a, b) {
            if (a.title < b.title) return -1;
            if (a.title > b.title) return 1;
            return 0
        })
        .forEach(function (link) {
            let iconic = (link.golden ? '★&nbsp;' : '') + (link.checked ? '✔&nbsp;' : '');
            let compiled = template(
                    '<%= icons %><a href="<%= Domain %>/<%= link.href %>"><%= link.title %></a>;' +
                    '<a href="<%= link.user_href %>"><%= link.user %></a>;' +
                    'рейтинг: <%= link.rating %>;комментарии: <%= link.comments %>;<%= new Date(link.last_updated*1000) %><br>\n'
            );

            let result = compiled({
                link: link,
                Domain: DOMAIN,
                icons: iconic
            });

            fs.appendFile(path.join(__dirname, '..', 'result.html'), result)
        });
}).call();